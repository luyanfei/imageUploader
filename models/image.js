const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ImageSchema = new Schema({
    title:          { type: String },
    description:    { type: String },
    filename:       { type: String },
    mimetype:       { type: String },
    originalname:   { type: String },
    views:          { type: Number, 'default': 0 },
    likes:          { type: Number, 'default': 0 },
    timestamp:      { type: Date, 'default': Date.now }
})

mongoose.model('Image', ImageSchema)
